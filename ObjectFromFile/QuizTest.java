package com.tanvir.file;

import java.awt.List;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class QuizTest {

	public static void main(String[] args) {
		FileReader fr = null;
		BufferedReader br = null;
		
		ArrayList<Quiz> quiz = new ArrayList<>();
		try {
			fr = new FileReader("src/com/tanvir/file/marks.txt");
			br = new BufferedReader(fr);
			
			String line;
			
			while((line = br.readLine()) != null) {
				String[] temp = line.split(" ");
				quiz.add(new Quiz(Integer.parseInt(temp[0]), Double.parseDouble(temp[1])));
			}
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		for(Quiz i: quiz) {
			System.out.println(i);
		}
	}	
}
