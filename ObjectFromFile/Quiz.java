package com.tanvir.file;

public class Quiz {
	private int id;
	private double marks;
	
	public Quiz(int id, double marks) {
		this.id = id;
		this.marks = marks;
	}

	public int getId() {
		return id;
	}

	public double getMarks() {
		return marks;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setMarks(double marks) {
		this.marks = marks;
	}

	@Override
	public String toString() {
		return "Quiz [id=" + id + ", marks=" + marks + "]";
	}
	
	
}
